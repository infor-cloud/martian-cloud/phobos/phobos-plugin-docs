// Copyright (c) The Phobos Authors
// SPDX-License-Identifier: MPL-2.0
// Copyright (c) HashiCorp, Inc.
// SPDX-License-Identifier: MPL-2.0

// Package main is the entrypoint for the phobos-plugin-docs binary.
package main

import (
	"os"

	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-plugin-docs/internal/cmd"
)

func main() {
	os.Exit(cmd.Main())
}
