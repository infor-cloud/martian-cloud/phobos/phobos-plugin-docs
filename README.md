# phobos-plugin-docs

This repository contains tools and packages for creating Phobos plugin docs (currently only plugin plugins).
The primary way users will interact with this is the `phobosplugindocs` CLI tool to generate and validate plugin documentation.

This project is based on https://github.com/hashicorp/terraform-plugin-docs

## `phobosplugindocs`

This tool will let you generate documentation for your plugin from live example `.tf` files and markdown templates.
It will also export schema information from the plugin (using `phobos plugins schema -json`),
and sync the schema with the reference documents.

If your documentation only consists of simple examples and schema information,
the tool can also generate missing template files to make website creation extremely simple for most plugins.


### Usage

```shell
$ phobosplugindocs --help
Usage: phobosplugindocs [--version] [--help] <command> [<args>]

Available commands are:
                the generate command is run by default
    generate    generates a plugin website from code, templates, and examples
```

`generate` command:

```shell
$ phobosplugindocs generate --help

Usage: phobosplugindocs generate [<args>]

    --examples-dir <ARG>             examples directory based on plugin-dir                                                                                           (default: "examples")
    --ignore-deprecated <ARG>        don't generate documentation for deprecated actions and data-sources                                                             (default: "false")
    --plugin-dir <ARG>             relative or absolute path to the root plugin code directory when running the command outside the root plugin code directory
    --plugin-name <ARG>            plugin name, as used in Phobos configurations
    --plugins-schema <ARG>         path to the plugins schema JSON file, which contains the output of the phobos plugins schema -json command. Setting this flag will skip building the plugin and calling Phobos CLI
    --rendered-plugin-name <ARG>   plugin name, as generated in documentation (ex. page titles, ...)
    --rendered-website-dir <ARG>     output directory based on plugin-dir                                                                                             (default: "docs")
    --website-source-dir <ARG>       templates directory based on plugin-dir                                                                                          (default: "templates")
    --website-temp-dir <ARG>         temporary directory (used during generation)
```

### How it Works

When you run `phobosplugindocs`, by default from the root directory of a plugin codebase, the tool takes the following actions:

* Copy all the templates and static files to a temporary directory
* Build (`go build`) a temporary binary of the plugin source code
* Collect schema information using `phobos plugins schema -json`
* Generate a default plugin template file, if missing (**index.md**)
* Generate action template files, if missing
* Generate data source template files, if missing
* Generate function template files, if missing (Requires Phobos v1.8.0+)
* Copy all non-template files to the output website directory
* Process all the remaining templates to generate files for the output website directory

### Conventional Paths

The generation of missing documentation is based on a number of assumptions / conventional paths.

For templates:

| Path                                                  | Description                            |
|-------------------------------------------------------|----------------------------------------|
| `templates/`                                          | Root of templated docs                 |
| `templates/index.md[.tmpl]`                           | Docs index page (or template)          |
| `templates/actions.md[.tmpl]`                         | Generic action page (or template)    |
| `templates/actions/<action name>.md[.tmpl]`           | Action page (or template)            |

Note: the `.tmpl` extension is necessary, for the file to be correctly handled as a template.

For examples:

| Path                                                      | Description                     |
|-----------------------------------------------------------|---------------------------------|
| `examples/`                                               | Root of examples                |
| `examples/plugin/plugin.hcl`                          | Plugin example config         |
| `examples/actions/<action name>/action.hcl`               | Action example config         |

### Templates

The templates are implemented with Go [`text/template`](https://golang.org/pkg/text/template/)
using the following data fields and functions:

#### Data fields

##### Plugin Fields

|                   Field |  Type  | Description                                                                               |
|------------------------:|:------:|-------------------------------------------------------------------------------------------|
|          `.Description` | string | Plugin description                                                                      |
|           `.HasExample` |  bool  | Is there an example file?                                                                 |
|          `.ExampleFile` | string | Path to the file with the phobos configuration example                                 |
|         `.PluginName` | string | Canonical plugin name (ex. `phobos-plugin-random`)                                 |
|    `.PluginShortName` | string | Short version of the plugin name (ex. `random`)                                         |
| `.RenderedPluginName` | string | Value provided via argument `--rendered-plugin-name`, otherwise same as `.PluginName` |
|       `.SchemaMarkdown` | string | a Markdown formatted Plugin Schema definition                                           |

##### Actions Fields

|                   Field |  Type  | Description                                                                               |
|------------------------:|:------:|-------------------------------------------------------------------------------------------|
|                 `.Name` | string | Name of the action/data-source (ex. `tls_certificate`)                                  |
|                 `.Type` | string | `Action`
|                `.Group` | string | Action group                                   |
|          `.Description` | string | Action description                                                        |
|           `.HasExample` |  bool  | Is there an example file?                                                                 |
|          `.ExampleFile` | string | Path to the file with the phobos configuration example                                 |
|            `.HasImport` |  bool  | Is there an import file?                                                                  |
|           `.ImportFile` | string | Path to the file with the command for importing the action                              |
|         `.PluginName` | string | Canonical plugin name (ex. `phobos-plugin-random`)                                 |
|    `.PluginShortName` | string | Short version of the plugin name (ex. `random`)                                         |
| `.RenderedPluginName` | string | Value provided via argument `--rendered-plugin-name`, otherwise same as `.PluginName` |
|       `.SchemaMarkdown` | string | a Markdown formatted Action Schema definition                             |

##### Plugin-defined Function Fields

|                               Field |  Type  | Description                                                                               |
|------------------------------------:|:------:|-------------------------------------------------------------------------------------------|
|                             `.Name` | string | Name of the function (ex. `echo`)                                                         |
|                             `.Type` | string | Returns `Function`                                                                        |
|                      `.Description` | string | Function description                                                                      |
|                          `.Summary` | string | Function summary                                                                          |
|                       `.HasExample` |  bool  | Is there an example file?                                                                 |
|                      `.ExampleFile` | string | Path to the file with the phobos configuration example                                 |
|                     `.PluginName` | string | Canonical plugin name (ex. `phobos-plugin-random`)                                 |
|                `.PluginShortName` | string | Short version of the plugin name (ex. `random`)                                         |
|             `.RenderedPluginName` | string | Value provided via argument `--rendered-plugin-name`, otherwise same as `.PluginName` |
|        `.FunctionSignatureMarkdown` | string | a Markdown formatted Function signature                                                   |
|        `.FunctionArgumentsMarkdown` | string | a Markdown formatted Function arguments definition                                        |
|                      `.HasVariadic` |  bool  | Does this function have a variadic argument?                                              |
| `.FunctionVariadicArgumentMarkdown` | string | a Markdown formatted Function variadic argument definition                                |

#### Template Functions

| Function        | Description                                                                                       |
|-----------------|---------------------------------------------------------------------------------------------------|
| `codefile`      | Create a Markdown code block with the content of a file. Path is relative to the repository root. |
| `lower`         | Equivalent to [`strings.ToLower`](https://pkg.go.dev/strings#ToLower).                            |
| `plainmarkdown` | Render Markdown content as plaintext.                                                             |
| `prefixlines`   | Add a prefix to all (newline-separated) lines in a string.                                        |
| `printf`        | Equivalent to [`fmt.Printf`](https://pkg.go.dev/fmt#Printf).                                      |
| `split`         | Split string into sub-strings, by a given separator (ex. `split .Name "_"`).                      |
| `title`         | Equivalent to [`cases.Title`](https://pkg.go.dev/golang.org/x/text/cases#Title).                  |
| `hclfile`        | A special case of the `codefile` function, designed for Phobos files (i.e. `.tf`).             |
| `trimspace`     | Equivalent to [`strings.TrimSpace`](https://pkg.go.dev/strings#TrimSpace).                        |
| `upper`         | Equivalent to [`strings.ToUpper`](https://pkg.go.dev/strings#ToUpper).                            |

### License Headers
All source code files in this repository (excluding autogenerated files like `go.mod`, prose, and files excluded in [.copywrite.hcl](.copywrite.hcl)) must have a license header at the top.

This can be autogenerated by running `make generate` or running `go generate ./...` in the [/tools](/tools) directory.
