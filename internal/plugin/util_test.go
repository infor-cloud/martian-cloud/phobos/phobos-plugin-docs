// Copyright (c) The Phobos Authors
// SPDX-License-Identifier: MPL-2.0
// Copyright (c) HashiCorp, Inc.
// SPDX-License-Identifier: MPL-2.0

package plugin

import (
	"testing"
)

func Test_extractSchemaFromFile(t *testing.T) {
	t.Parallel()

	filepath := "testdata/schema.json"
	schema, err := extractSchemaFromFile(filepath)
	if err != nil {
		t.Errorf("received error %v:", err)
	}

	if schema.PipelineSchema.Config == nil {
		t.Fatalf("plugin config not found")
	}

	if schema.PipelineSchema.Actions["action1"] == nil {
		t.Fatalf("action1 not found")
	}
}
