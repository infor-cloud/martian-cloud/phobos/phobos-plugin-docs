// Copyright (c) The Phobos Authors
// SPDX-License-Identifier: MPL-2.0
// Copyright (c) HashiCorp, Inc.
// SPDX-License-Identifier: MPL-2.0

// Package plugin provides the ability to generate static documentation for a Phobos plugin.
package plugin

import (
	"context"
	"fmt"
	"os"
	"path"
	"path/filepath"
	"strings"

	"github.com/hashicorp/cli"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-plugin-sdk/internal-shared/schema"
	"golang.org/x/exp/slices"
)

var (
	websiteActionFile                 = "actions/%s.md.tmpl"
	websiteActionFallbackFile         = "actions.md.tmpl"
	websiteActionFileStaticCandidates = []string{
		"actions/%s.md",
		"actions/%s.markdown",
		"actions/%s.html.markdown",
		"actions/%s.html.md",
	}
	websitePluginFile                 = "index.md.tmpl"
	websitePluginFileStaticCandidates = []string{
		"index.markdown",
		"index.md",
		"index.html.markdown",
		"index.html.md",
	}

	managedWebsiteSubDirectories = []string{
		"guides",
		"actions",
	}

	managedWebsiteFiles = []string{
		"index.md",
	}
)

type generator struct {
	ignoreDeprecated bool

	// pluginDir is the absolute path to the root plugin directory
	pluginDir string

	pluginName string

	pluginSchemaPath   string
	renderedWebsiteDir string
	examplesDir        string
	templatesDir       string
	websiteTmpDir      string

	ui cli.Ui
}

func (g *generator) infof(format string, a ...interface{}) {
	g.ui.Info(fmt.Sprintf(format, a...))
}

func (g *generator) warnf(format string, a ...interface{}) {
	g.ui.Warn(fmt.Sprintf(format, a...))
}

// Generate generates markdown for the given plugin
func Generate(ui cli.Ui, pluginDir, renderedWebsiteDir, examplesDir, websiteTmpDir, templatesDir string, ignoreDeprecated bool) error {
	// Ensure plugin directory is resolved absolute path
	if pluginDir == "" {
		wd, err := os.Getwd()

		if err != nil {
			return fmt.Errorf("error getting working directory: %w", err)
		}

		pluginDir = wd
	} else {
		absPluginDir, err := filepath.Abs(pluginDir)

		if err != nil {
			return fmt.Errorf("error getting absolute path with plugin directory %q: %w", pluginDir, err)
		}

		pluginDir = absPluginDir
	}

	// Verify plugin directory
	pluginDirFileInfo, err := os.Stat(pluginDir)
	if err != nil {
		return fmt.Errorf("error getting information for plugin directory %q: %w", pluginDir, err)
	}

	if !pluginDirFileInfo.IsDir() {
		return fmt.Errorf("expected %q to be a directory", pluginDir)
	}

	pluginName := filepath.Base(pluginDir)

	g := &generator{
		ignoreDeprecated: ignoreDeprecated,

		pluginDir:          pluginDir,
		pluginName:         pluginName,
		pluginSchemaPath:   filepath.Join(pluginDir, "schema.json"),
		renderedWebsiteDir: renderedWebsiteDir,
		examplesDir:        examplesDir,
		templatesDir:       templatesDir,
		websiteTmpDir:      websiteTmpDir,

		ui: ui,
	}

	ctx := context.Background()

	return g.Generate(ctx)
}

// Generate generates markdown for the given plugin
func (g *generator) Generate(_ context.Context) error {
	var err error

	g.infof("rendering website for plugin %q", g.pluginName)

	switch {
	case g.websiteTmpDir == "":
		g.websiteTmpDir, err = os.MkdirTemp("", "phobosdocs")
		if err != nil {
			return fmt.Errorf("error creating temporary website directory: %w", err)
		}
		defer os.RemoveAll(g.websiteTmpDir)
	default:
		g.infof("cleaning tmp dir %q", g.websiteTmpDir)
		err = os.RemoveAll(g.websiteTmpDir)
		if err != nil {
			return fmt.Errorf("error removing temporary website directory %q: %w", g.websiteTmpDir, err)
		}

		g.infof("creating tmp dir %q", g.websiteTmpDir)
		err = os.MkdirAll(g.websiteTmpDir, 0755) // nosemgrep: gosec.G301-1
		if err != nil {
			return fmt.Errorf("error creating temporary website directory %q: %w", g.websiteTmpDir, err)
		}
	}

	templatesDirInfo, err := os.Stat(g.PluginTemplatesDir())
	switch {
	case os.IsNotExist(err):
		// do nothing, no template dir
	case err != nil:
		return fmt.Errorf("error getting information for plugin templates directory %q: %w", g.PluginTemplatesDir(), err)
	default:
		if !templatesDirInfo.IsDir() {
			return fmt.Errorf("template path is not a directory: %s", g.PluginTemplatesDir())
		}

		g.infof("copying any existing content to tmp dir")
		err = cp(g.PluginTemplatesDir(), g.TempTemplatesDir())
		if err != nil {
			return fmt.Errorf("error copying exiting content to temporary directory %q: %w", g.TempTemplatesDir(), err)
		}
	}

	g.infof("exporting schema from JSON file")
	pluginSchema, err := g.pluginSchemaFromFile()
	if err != nil {
		return fmt.Errorf("error exporting plugin schema from JSON file: %w", err)
	}

	g.infof("generating missing templates")
	err = g.generateMissingTemplates(pluginSchema)
	if err != nil {
		return fmt.Errorf("error generating missing templates: %w", err)
	}

	g.infof("rendering static website")
	err = g.renderStaticWebsite(pluginSchema)
	if err != nil {
		return fmt.Errorf("error rendering static website: %w", err)
	}

	return nil
}

// PluginDocsDir returns the absolute path to the joined plugin and
// given website documentation directory, which defaults to "docs".
func (g *generator) PluginDocsDir() string {
	return filepath.Join(g.pluginDir, g.renderedWebsiteDir)
}

// PluginExamplesDir returns the absolute path to the joined plugin and
// given examples directory, which defaults to "examples".
func (g *generator) PluginExamplesDir() string {
	return filepath.Join(g.pluginDir, g.examplesDir)
}

// PluginTemplatesDir returns the absolute path to the joined plugin and
// given templates directory, which defaults to "templates".
func (g *generator) PluginTemplatesDir() string {
	return filepath.Join(g.pluginDir, g.templatesDir)
}

// TempTemplatesDir returns the absolute path to the joined temporary and
// hardcoded "templates" subdirectory, which is where plugin templates are
// copied.
func (g *generator) TempTemplatesDir() string {
	return filepath.Join(g.websiteTmpDir, "templates")
}

func (g *generator) generateMissingActionTemplate(actionName string) error {
	templatePath := fmt.Sprintf(websiteActionFile, actionShortName(actionName, g.pluginName))
	templatePath = filepath.Join(g.TempTemplatesDir(), templatePath)
	if fileExists(templatePath) {
		g.infof("action %q template exists, skipping", actionName)
		return nil
	}

	fallbackTemplatePath := filepath.Join(g.TempTemplatesDir(), websiteActionFallbackFile)
	if fileExists(fallbackTemplatePath) {
		g.infof("action %q fallback template exists, creating template", actionName)
		err := cp(fallbackTemplatePath, templatePath)
		if err != nil {
			return fmt.Errorf("unable to copy fallback template for %q: %w", actionName, err)
		}
		return nil
	}

	for _, candidate := range websiteActionFileStaticCandidates {
		candidatePath := fmt.Sprintf(candidate, actionShortName(actionName, g.pluginName))
		candidatePath = filepath.Join(g.TempTemplatesDir(), candidatePath)
		if fileExists(candidatePath) {
			g.infof("action %q static file exists, skipping", actionName)
			return nil
		}
	}

	g.infof("generating new template for %q", actionName)
	err := writeFile(templatePath, string(defaultActionTemplate))
	if err != nil {
		return fmt.Errorf("unable to write template for %q: %w", actionName, err)
	}

	return nil
}

func (g *generator) generateMissingPluginTemplate() error {
	templatePath := filepath.Join(g.TempTemplatesDir(), websitePluginFile)
	if fileExists(templatePath) {
		g.infof("plugin %q template exists, skipping", g.pluginName)
		return nil
	}

	for _, candidate := range websitePluginFileStaticCandidates {
		candidatePath := filepath.Join(g.TempTemplatesDir(), candidate)
		if fileExists(candidatePath) {
			g.infof("plugin %q static file exists, skipping", g.pluginName)
			return nil
		}
	}

	g.infof("generating new template for %q", g.pluginName)
	err := writeFile(templatePath, string(defaultPluginTemplate))
	if err != nil {
		return fmt.Errorf("unable to write template for %q: %w", g.pluginName, err)
	}

	return nil
}

func (g *generator) generateMissingTemplates(pluginSchema *schema.PluginSchema) error {
	g.infof("generating missing action content")
	for name, schema := range pluginSchema.PipelineSchema.Actions {
		if g.ignoreDeprecated && schema.Deprecated {
			continue
		}

		err := g.generateMissingActionTemplate(name)
		if err != nil {
			return fmt.Errorf("unable to generate template for action %q: %w", name, err)
		}
	}

	g.infof("generating missing plugin content")
	err := g.generateMissingPluginTemplate()
	if err != nil {
		return fmt.Errorf("unable to generate template for plugin: %w", err)
	}

	return nil
}

func (g *generator) renderStaticWebsite(pluginSchema *schema.PluginSchema) error {
	g.infof("cleaning rendered website dir")
	dirEntry, err := os.ReadDir(g.PluginDocsDir())
	if err != nil && !os.IsNotExist(err) {
		return fmt.Errorf("unable to read rendered website directory %q: %w", g.PluginDocsDir(), err)
	}

	for _, file := range dirEntry {

		// Remove subdirectories managed by phobosplugindocs
		if file.IsDir() && slices.Contains(managedWebsiteSubDirectories, file.Name()) {
			g.infof("removing directory: %q", file.Name())
			err = os.RemoveAll(path.Join(g.PluginDocsDir(), file.Name()))
			if err != nil {
				return fmt.Errorf("unable to remove directory %q from rendered website directory: %w", file.Name(), err)
			}
			continue
		}

		// Remove files managed by phobosplugindocs
		if !file.IsDir() && slices.Contains(managedWebsiteFiles, file.Name()) {
			g.infof("removing file: %q", file.Name())
			err = os.RemoveAll(path.Join(g.PluginDocsDir(), file.Name()))
			if err != nil {
				return fmt.Errorf("unable to remove file %q from rendered website directory: %w", file.Name(), err)
			}
			continue
		}
	}

	shortName := pluginShortName(g.pluginName)

	g.infof("rendering templated website to static markdown")

	err = filepath.WalkDir(g.websiteTmpDir, func(path string, d os.DirEntry, err error) error {
		if err != nil {
			return fmt.Errorf("unable to walk path %q: %w", path, err)
		}
		if d.IsDir() {
			// skip directories
			return nil
		}

		rel, err := filepath.Rel(filepath.Join(g.TempTemplatesDir()), path)
		if err != nil {
			return fmt.Errorf("unable to retrieve the relative path of basepath %q and targetpath %q: %w",
				filepath.Join(g.TempTemplatesDir()), path, err)
		}

		relDir, relFile := filepath.Split(rel)
		relDir = filepath.ToSlash(relDir)

		// skip special top-level generic action, data source, and function templates
		if relDir == "" && (relFile == "actions.md.tmpl") {
			return nil
		}

		renderedPath := filepath.Join(g.PluginDocsDir(), rel)
		err = os.MkdirAll(filepath.Dir(renderedPath), 0755) // nosemgrep: gosec.G301-1
		if err != nil {
			return fmt.Errorf("unable to create rendered website subdirectory %q: %w", renderedPath, err)
		}

		ext := filepath.Ext(path)
		if ext != ".tmpl" {
			g.infof("copying non-template file: %q", rel)
			return cp(path, renderedPath)
		}

		renderedPath = strings.TrimSuffix(renderedPath, ext)

		fmt.Println(path)

		tmplData, err := os.ReadFile(path)
		if err != nil {
			return fmt.Errorf("unable to read file %q: %w", rel, err)
		}

		out, err := os.Create(renderedPath)
		if err != nil {
			return fmt.Errorf("unable to create file %q: %w", renderedPath, err)
		}
		defer out.Close()

		g.infof("rendering %q", rel)
		switch relDir {
		case "actions/":
			actionName := removeAllExt(relFile)

			exampleFilePath := filepath.Join(g.PluginExamplesDir(), "actions", actionName, "action.hcl")

			schema, ok := pluginSchema.PipelineSchema.Actions[actionName]

			if ok {
				tmpl := actionTemplate(tmplData)
				render, err := tmpl.Render(g.pluginDir, actionName, "Action", exampleFilePath, g.pluginName, schema)
				if err != nil {
					return fmt.Errorf("unable to render action template %q: %w", rel, err)
				}
				_, err = out.WriteString(render)
				if err != nil {
					return fmt.Errorf("unable to write rendered string: %w", err)
				}
				return nil
			}
			g.warnf("action entitled %q, or %q does not exist", shortName, actionName)
		case "": // plugin
			if relFile == "index.md.tmpl" {
				tmpl := pluginTemplate(tmplData)
				exampleFilePath := filepath.Join(g.PluginExamplesDir(), "plugin", "plugin.hcl")
				render, err := tmpl.Render(g.pluginDir, exampleFilePath, g.pluginName, pluginSchema)
				if err != nil {
					return fmt.Errorf("unable to render plugin template %q: %w", rel, err)
				}
				_, err = out.WriteString(render)
				if err != nil {
					return fmt.Errorf("unable to write rendered string: %w", err)
				}
				return nil
			}
		}

		tmpl := docTemplate(tmplData)
		err = tmpl.Render(g.pluginDir, out)
		if err != nil {
			return fmt.Errorf("unable to render template %q: %w", rel, err)
		}
		return nil
	})
	if err != nil {
		return fmt.Errorf("unable to render templated website to static markdown: %w", err)
	}

	return nil
}

func (g *generator) pluginSchemaFromFile() (*schema.PluginSchema, error) {
	g.infof("getting plugin schema")
	schema, err := extractSchemaFromFile(g.pluginSchemaPath)
	if err != nil {
		return nil, fmt.Errorf("unable to retrieve plugin schema from JSON file: %w", err)
	}

	return schema, nil
}
