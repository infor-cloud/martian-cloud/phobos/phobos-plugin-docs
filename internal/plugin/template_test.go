// Copyright (c) The Phobos Authors
// SPDX-License-Identifier: MPL-2.0
// Copyright (c) HashiCorp, Inc.
// SPDX-License-Identifier: MPL-2.0

package plugin

import (
	"strings"
	"testing"

	"github.com/google/go-cmp/cmp"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-plugin-sdk/internal-shared/component"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-plugin-sdk/internal-shared/schema"
)

func TestRenderStringTemplate(t *testing.T) {
	t.Parallel()

	template := `
Plainmarkdown: {{ plainmarkdown .Text }}
Split: {{ $arr := split .Text " "}}{{ index $arr 3 }}
Trimspace: {{ trimspace .Text }}
Lower: {{ upper .Text }}
Upper: {{ lower .Text }}
Title: {{ title .Text }}
Prefixlines:
{{ prefixlines "  " .MultiLineTest }}
Printf hclfile: {{ printf "{{hclfile %q}}" .Code }}
hclfile: {{ hclfile .Code }}
`

	expectedString := `
Plainmarkdown: my Odly cAsed striNg
Split: striNg
Trimspace: my Odly cAsed striNg
Lower: MY ODLY CASED STRING
Upper: my odly cased string
Title: My Odly Cased String
Prefixlines:
  This text used
  multiple lines
Printf hclfile: {{hclfile "plugin.hcl"}}
hclfile: hcl
plugin "my_plugin" {
  # example configuration here
}

`
	result, err := renderStringTemplate("testdata/test-plugin-dir", "testTemplate", template, struct {
		Text          string
		MultiLineTest string
		Code          string
	}{
		Text: "my Odly cAsed striNg",
		MultiLineTest: `This text used
multiple lines`,
		Code: "plugin.hcl",
	})

	if err != nil {
		t.Error(err)
	}
	cleanedResult := strings.ReplaceAll(result, "```", "")
	if !cmp.Equal(expectedString, cleanedResult) {
		t.Errorf("expected: %+v, got: %+v", expectedString, cleanedResult)
	}
}

func TestActionTemplate_Render(t *testing.T) {
	t.Parallel()

	template := `
Printf hclfile: {{ printf "{{hclfile %q}}" .ExampleFile }}
hclfile: {{ hclfile .ExampleFile }}
`
	expectedString := `
Printf hclfile: {{hclfile "plugin.hcl"}}
hclfile: hcl
plugin "my_plugin" {
  # example configuration here
}

`

	tpl := actionTemplate(template)

	schema := component.PipelinePluginActionSchema{
		Name:    "action1",
		Input:   &component.SchemaBlock{},
		Outputs: map[string]*component.SchemaAttribute{},
	}

	result, err := tpl.Render("testdata/test-plugin-dir", "testTemplate", "Action", "plugin.hcl", "phobos-plugin-test", &schema)
	if err != nil {
		t.Error(err)
	}

	cleanedResult := strings.ReplaceAll(result, "```", "")
	if !cmp.Equal(expectedString, cleanedResult) {
		t.Errorf("expected: %+v, got: %+v", expectedString, cleanedResult)
	}
}

func TestPluginTemplate_Render(t *testing.T) {
	t.Parallel()

	template := `
Printf hclfile: {{ printf "{{hclfile %q}}" .ExampleFile }}
hclfile: {{ hclfile .ExampleFile }}
`
	expectedString := `
Printf hclfile: {{hclfile "plugin.hcl"}}
hclfile: hcl
plugin "my_plugin" {
  # example configuration here
}

`

	tpl := pluginTemplate(template)

	schema := schema.PluginSchema{
		FormatVersion: "1.0",
		PipelineSchema: &component.PipelinePluginSchema{
			Config: &component.SchemaBlock{
				Attributes: nil,
				BlockSpecs: nil,
			},
		},
	}

	result, err := tpl.Render("testdata/test-plugin-dir", "plugin.hcl", "phobos-plugin-test", &schema)
	if err != nil {
		t.Error(err)
	}

	cleanedResult := strings.ReplaceAll(result, "```", "")
	if !cmp.Equal(expectedString, cleanedResult) {
		t.Errorf("expected: %+v, got: %+v", expectedString, cleanedResult)
	}
}
