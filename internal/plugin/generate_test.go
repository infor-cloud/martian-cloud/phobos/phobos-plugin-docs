// Copyright (c) The Phobos Authors
// SPDX-License-Identifier: MPL-2.0
// Copyright (c) HashiCorp, Inc.
// SPDX-License-Identifier: MPL-2.0

package plugin

import (
	"testing"

	"github.com/hashicorp/cli"
)

func TestGenerator_pluginSchemaFromFile(t *testing.T) {
	t.Parallel()

	g := &generator{
		pluginSchemaPath: "testdata/schema.json",
		ui:               cli.NewMockUi(),
	}

	schema, err := g.pluginSchemaFromFile()
	if err != nil {
		t.Fatalf("error retrieving schema: %q", err)
	}

	if schema.PipelineSchema.Actions["action1"] == nil {
		t.Fatalf("action not found")
	}
}
