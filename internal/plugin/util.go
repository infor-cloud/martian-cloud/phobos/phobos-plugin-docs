// Copyright (c) The Phobos Authors
// SPDX-License-Identifier: MPL-2.0
// Copyright (c) HashiCorp, Inc.
// SPDX-License-Identifier: MPL-2.0

package plugin

import (
	"encoding/json"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"strings"

	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-plugin-sdk/internal-shared/schema"
)

func pluginShortName(n string) string {
	return strings.TrimPrefix(n, "phobos-plugin-")
}

func actionShortName(name, pluginName string) string {
	psn := pluginShortName(pluginName)
	return strings.TrimPrefix(name, psn+"_")
}

func copyFile(srcPath, dstPath string, mode os.FileMode) error {
	srcFile, err := os.Open(srcPath)
	if err != nil {
		return err
	}
	defer srcFile.Close()

	// Ensure destination path exists for file creation
	err = os.MkdirAll(filepath.Dir(dstPath), 0755) // nosemgrep: gosec.G301-1
	if err != nil {
		return err
	}

	// If the destination file already exists, we shouldn't blow it away
	dstFile, err := os.OpenFile(dstPath, os.O_WRONLY|os.O_CREATE|os.O_EXCL, mode)
	if err != nil {
		return err
	}
	defer dstFile.Close()

	_, err = io.Copy(dstFile, srcFile)
	if err != nil {
		return err
	}

	return nil
}

func removeAllExt(file string) string {
	for {
		ext := filepath.Ext(file)
		if ext == "" || ext == file {
			return file
		}
		file = strings.TrimSuffix(file, ext)
	}
}

func writeFile(path string, data string) error {
	dir, _ := filepath.Split(path)

	var err error
	if dir != "" {
		err = os.MkdirAll(dir, 0755) // nosemgrep: gosec.G301-1
		if err != nil {
			return fmt.Errorf("unable to make dir %q: %w", dir, err)
		}
	}

	err = os.WriteFile(path, []byte(data), 0600)
	if err != nil {
		return fmt.Errorf("unable to write file %q: %w", path, err)
	}

	return nil
}

func cp(srcDir, dstDir string) error {
	err := filepath.Walk(srcDir, func(srcPath string, f os.FileInfo, err error) error {
		if err != nil {
			return err
		}

		relPath, err := filepath.Rel(srcDir, srcPath)
		if err != nil {
			return err
		}

		dstPath := filepath.Join(dstDir, relPath)

		switch mode := f.Mode(); {
		case mode.IsDir():
			if err := os.Mkdir(dstPath, f.Mode()); err != nil && !os.IsExist(err) {
				return err
			}
		case mode.IsRegular():
			if err := copyFile(srcPath, dstPath, mode); err != nil {
				return err
			}
		default:
			return fmt.Errorf("unknown file type (%d / %s) for %s", f.Mode(), f.Mode().String(), srcPath)
		}

		return nil
	})
	return err
}

func fileExists(filename string) bool {
	info, err := os.Stat(filename)
	if os.IsNotExist(err) {
		return false
	}
	return !info.IsDir()
}

func extractSchemaFromFile(path string) (*schema.PluginSchema, error) {
	schemajson, err := os.ReadFile(path)
	if err != nil {
		return nil, fmt.Errorf("unable to read file %q: %w", path, err)
	}

	schema := &schema.PluginSchema{}
	err = json.Unmarshal(schemajson, schema)
	if err != nil {
		return nil, err
	}

	return schema, nil
}
