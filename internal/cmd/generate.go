// Copyright (c) The Phobos Authors
// SPDX-License-Identifier: MPL-2.0
// Copyright (c) HashiCorp, Inc.
// SPDX-License-Identifier: MPL-2.0

package cmd

import (
	"flag"
	"fmt"
	"strings"

	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-plugin-docs/internal/plugin"
)

type generateCmd struct {
	commonCmd

	flagIgnoreDeprecated bool

	flagPluginDir          string
	flagRenderedWebsiteDir string
	flagExamplesDir        string
	flagWebsiteTmpDir      string
	flagWebsiteSourceDir   string
}

func (cmd *generateCmd) Synopsis() string {
	return "generates a plugin website from code, templates, and examples"
}

func (cmd *generateCmd) Help() string {
	strBuilder := &strings.Builder{}

	longestName := 0
	longestUsage := 0
	cmd.Flags().VisitAll(func(f *flag.Flag) {
		if len(f.Name) > longestName {
			longestName = len(f.Name)
		}
		if len(f.Usage) > longestUsage {
			longestUsage = len(f.Usage)
		}
	})

	strBuilder.WriteString("\nUsage: phobosplugindocs generate [<args>]\n\n")
	cmd.Flags().VisitAll(func(f *flag.Flag) {
		if f.DefValue != "" {
			strBuilder.WriteString(fmt.Sprintf("    --%s <ARG> %s%s%s  (default: %q)\n",
				f.Name,
				strings.Repeat(" ", longestName-len(f.Name)+2),
				f.Usage,
				strings.Repeat(" ", longestUsage-len(f.Usage)+2),
				f.DefValue,
			))
		} else {
			strBuilder.WriteString(fmt.Sprintf("    --%s <ARG> %s%s%s\n",
				f.Name,
				strings.Repeat(" ", longestName-len(f.Name)+2),
				f.Usage,
				strings.Repeat(" ", longestUsage-len(f.Usage)+2),
			))
		}
	})
	strBuilder.WriteString("\n")

	return strBuilder.String()
}

func (cmd *generateCmd) Flags() *flag.FlagSet {
	fs := flag.NewFlagSet("generate", flag.ExitOnError)
	fs.StringVar(&cmd.flagPluginDir, "plugin-dir", "", "relative or absolute path to the root plugin code directory when running the command outside the root plugin code directory")
	fs.StringVar(&cmd.flagRenderedWebsiteDir, "rendered-website-dir", "docs", "output directory based on plugin-dir")
	fs.StringVar(&cmd.flagExamplesDir, "examples-dir", "examples", "examples directory based on plugin-dir")
	fs.StringVar(&cmd.flagWebsiteTmpDir, "website-temp-dir", "", "temporary directory (used during generation)")
	fs.StringVar(&cmd.flagWebsiteSourceDir, "website-source-dir", "templates", "templates directory based on plugin-dir")
	fs.BoolVar(&cmd.flagIgnoreDeprecated, "ignore-deprecated", false, "don't generate documentation for deprecated actions")
	return fs
}

func (cmd *generateCmd) Run(args []string) int {
	fs := cmd.Flags()
	err := fs.Parse(args)
	if err != nil {
		cmd.ui.Error(fmt.Sprintf("unable to parse flags: %s", err))
		return 1
	}

	return cmd.run(cmd.runInternal)
}

func (cmd *generateCmd) runInternal() error {
	err := plugin.Generate(
		cmd.ui,
		cmd.flagPluginDir,
		cmd.flagRenderedWebsiteDir,
		cmd.flagExamplesDir,
		cmd.flagWebsiteTmpDir,
		cmd.flagWebsiteSourceDir,
		cmd.flagIgnoreDeprecated,
	)
	if err != nil {
		return fmt.Errorf("unable to generate website: %w", err)
	}

	return nil
}
