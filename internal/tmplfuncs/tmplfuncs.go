// Copyright (c) The Phobos Authors
// SPDX-License-Identifier: MPL-2.0
// Copyright (c) HashiCorp, Inc.
// SPDX-License-Identifier: MPL-2.0

// Package tmplfuncs provides custom template functions for use in the markdown
package tmplfuncs

import (
	"fmt"
	"os"
	"strings"
)

// PrefixLines prefixes each line in the text with the given prefix
func PrefixLines(prefix, text string) string {
	return prefix + strings.Join(strings.Split(text, "\n"), "\n"+prefix)
}

// CodeFile reads the content of a file and returns it as a markdown code block
func CodeFile(format, file string) (string, error) {
	content, err := os.ReadFile(file)
	if err != nil {
		return "", fmt.Errorf("unable to read content from %q: %w", file, err)
	}

	sContent := strings.TrimSpace(string(content))
	if sContent == "" {
		return "", fmt.Errorf("no file content in %q", file)
	}

	md := &strings.Builder{}
	_, err = md.WriteString("```" + format + "\n")
	if err != nil {
		return "", err
	}

	_, err = md.WriteString(sContent)
	if err != nil {
		return "", err
	}

	_, err = md.WriteString("\n```")
	if err != nil {
		return "", err
	}

	return md.String(), nil
}
