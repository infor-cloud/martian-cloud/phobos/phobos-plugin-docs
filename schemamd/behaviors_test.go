// Copyright (c) The Phobos Authors
// SPDX-License-Identifier: MPL-2.0
// Copyright (c) HashiCorp, Inc.
// SPDX-License-Identifier: MPL-2.0

package schemamd

import (
	"testing"

	"github.com/aws/smithy-go/ptr"
	"github.com/google/go-cmp/cmp"
	"github.com/zclconf/go-cty/cty"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-plugin-sdk/internal-shared/component"
)

func TestChildAttributeIsRequired(t *testing.T) {
	t.Parallel()

	for _, c := range []struct {
		name     string
		att      *component.SchemaAttribute
		expected bool
	}{
		{
			"required",
			&component.SchemaAttribute{
				Type:        cty.String,
				Required:    ptr.Bool(true),
				Description: "This is an attribute.",
			},
			true,
		},
		{
			"not required",
			&component.SchemaAttribute{
				Type:        cty.String,
				Required:    ptr.Bool(false),
				Description: "This is an attribute.",
			},
			false,
		},
	} {
		c := c
		t.Run(c.name, func(t *testing.T) {
			t.Parallel()

			actual := childAttributeIsRequired(c.att)
			if diff := cmp.Diff(c.expected, actual); diff != "" {
				t.Fatalf("Unexpected diff (-wanted, +got): %s", diff)
			}
		})
	}
}

func TestChildAttributeIsOptional(t *testing.T) {
	t.Parallel()

	for _, c := range []struct {
		name     string
		att      *component.SchemaAttribute
		expected bool
	}{
		{
			"required",
			&component.SchemaAttribute{
				Type:        cty.String,
				Required:    ptr.Bool(true),
				Description: "This is an attribute.",
			},
			false,
		},
		{
			"not required",
			&component.SchemaAttribute{
				Type:        cty.String,
				Required:    ptr.Bool(false),
				Description: "This is an attribute.",
			},
			true,
		},
	} {
		c := c
		t.Run(c.name, func(t *testing.T) {
			t.Parallel()

			actual := childAttributeIsOptional(c.att)
			if diff := cmp.Diff(c.expected, actual); diff != "" {
				t.Fatalf("Unexpected diff (-wanted, +got): %s", diff)
			}
		})
	}
}

func TestChildBlockIsRequired(t *testing.T) {
	t.Parallel()

	for _, c := range []struct {
		name     string
		block    *component.SchemaBlockSpec
		expected bool
	}{
		{
			"required",
			&component.SchemaBlockSpec{
				Required: true,
			},
			true,
		},
		{
			"not required",
			&component.SchemaBlockSpec{
				Required: false,
			},
			false,
		},
	} {
		c := c
		t.Run(c.name, func(t *testing.T) {
			t.Parallel()

			actual := childBlockIsRequired(c.block)
			if diff := cmp.Diff(c.expected, actual); diff != "" {
				t.Fatalf("Unexpected diff (-wanted, +got): %s", diff)
			}
		})
	}
}

func TestChildBlockIsOptional(t *testing.T) {
	t.Parallel()

	for _, c := range []struct {
		name     string
		block    *component.SchemaBlockSpec
		expected bool
	}{
		{
			"required",
			&component.SchemaBlockSpec{
				Required: true,
			},
			false,
		},
		{
			"not required",
			&component.SchemaBlockSpec{
				Required: false,
			},
			true,
		},
	} {
		c := c
		t.Run(c.name, func(t *testing.T) {
			t.Parallel()

			actual := childBlockIsOptional(c.block)
			if diff := cmp.Diff(c.expected, actual); diff != "" {
				t.Fatalf("Unexpected diff (-wanted, +got): %s", diff)
			}
		})
	}
}
