// Copyright (c) The Phobos Authors
// SPDX-License-Identifier: MPL-2.0
// Copyright (c) HashiCorp, Inc.
// SPDX-License-Identifier: MPL-2.0

package schemamd

import (
	"fmt"
	"io"
	"strings"

	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-plugin-sdk/internal-shared/component"
)

// WriteAttributeDescription writes the description of an attribute
func WriteAttributeDescription(w io.Writer, att *component.SchemaAttribute, includeRW bool) error {
	_, err := io.WriteString(w, "(")
	if err != nil {
		return err
	}

	err = WriteType(w, att.Type)
	if err != nil {
		return err
	}

	if includeRW {
		switch {
		case childAttributeIsRequired(att):
			_, err = io.WriteString(w, ", Required")
			if err != nil {
				return err
			}
		case childAttributeIsOptional(att):
			_, err = io.WriteString(w, ", Optional")
			if err != nil {
				return err
			}
		default:
			return fmt.Errorf("attribute does not match any filter states")
		}
	}

	if att.Deprecated {
		_, err := io.WriteString(w, ", Deprecated")
		if err != nil {
			return err
		}
	}

	_, err = io.WriteString(w, ")")
	if err != nil {
		return err
	}

	desc := strings.TrimSpace(att.Description)
	if desc != "" {
		_, err = io.WriteString(w, " "+desc)
		if err != nil {
			return err
		}
	}

	return nil
}
