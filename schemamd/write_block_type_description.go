// Copyright (c) The Phobos Authors
// SPDX-License-Identifier: MPL-2.0
// Copyright (c) HashiCorp, Inc.
// SPDX-License-Identifier: MPL-2.0

package schemamd

import (
	"fmt"
	"io"
	"strings"

	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-plugin-sdk/internal-shared/component"
)

// WriteBlockTypeDescription writes the description of a block spec field
func WriteBlockTypeDescription(w io.Writer, block *component.SchemaBlockSpec) error {
	_, err := io.WriteString(w, "(Block")
	if err != nil {
		return err
	}

	switch {
	case block.Type.IsObjectType():
		// nothing
	case block.Type.IsListType():
		_, err = io.WriteString(w, " List")
		if err != nil {
			return err
		}
	case block.Type.IsMapType():
		_, err = io.WriteString(w, " Map")
		if err != nil {
			return err
		}
	default:
		return fmt.Errorf("unexpected type for block: %s", block.Type.FriendlyName())
	}

	if block.Required {
		_, err = io.WriteString(w, ", Required")
		if err != nil {
			return err
		}
	} else {
		_, err = io.WriteString(w, ", Optional")
		if err != nil {
			return err
		}
	}

	if block.Deprecated {
		_, err = io.WriteString(w, ", Deprecated")
		if err != nil {
			return err
		}
	}

	_, err = io.WriteString(w, ")")
	if err != nil {
		return err
	}

	desc := strings.TrimSpace(block.Description)
	if desc != "" {
		_, err = io.WriteString(w, " "+desc)
		if err != nil {
			return err
		}
	}

	return nil
}
