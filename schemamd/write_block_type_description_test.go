// Copyright (c) The Phobos Authors
// SPDX-License-Identifier: MPL-2.0
// Copyright (c) HashiCorp, Inc.
// SPDX-License-Identifier: MPL-2.0

package schemamd_test

import (
	"strings"
	"testing"

	"github.com/google/go-cmp/cmp"
	"github.com/zclconf/go-cty/cty"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-plugin-docs/schemamd"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-plugin-sdk/internal-shared/component"
)

func TestWriteBlockTypeDescription(t *testing.T) {
	t.Parallel()

	for _, c := range []struct {
		expected string
		bt       *component.SchemaBlockSpec
	}{
		// single
		{
			"(Block, Optional) This is a block.",
			&component.SchemaBlockSpec{
				Type:        cty.EmptyObject,
				Description: "This is a block.",
			},
		},
		{
			"(Block, Required) This is a block.",
			&component.SchemaBlockSpec{
				Type:        cty.EmptyObject,
				Description: "This is a block.",
				Required:    true,
			},
		},
		{
			"(Block, Required, Deprecated) This is a block.",
			&component.SchemaBlockSpec{
				Type:        cty.EmptyObject,
				Description: "This is a block.",
				Required:    true,
				Deprecated:  true,
			},
		},

		// list
		{
			"(Block List, Optional) This is a block.",
			&component.SchemaBlockSpec{
				Type:        cty.List(cty.EmptyObject),
				Description: "This is a block.",
				Required:    false,
			},
		},

		// map
		{
			"(Block Map, Optional) This is a block.",
			&component.SchemaBlockSpec{
				Type:        cty.Map(cty.EmptyObject),
				Description: "This is a block.",
				Required:    false,
			},
		},
	} {
		c := c
		t.Run(c.expected, func(t *testing.T) {
			t.Parallel()

			b := &strings.Builder{}
			err := schemamd.WriteBlockTypeDescription(b, c.bt)
			if err != nil {
				t.Fatal(err)
			}
			actual := b.String()
			if diff := cmp.Diff(c.expected, actual); diff != "" {
				t.Fatalf("Unexpected diff (-wanted, +got): %s", diff)
			}
		})
	}
}
