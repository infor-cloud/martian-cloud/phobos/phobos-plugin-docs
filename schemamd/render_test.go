// Copyright (c) The Phobos Authors
// SPDX-License-Identifier: MPL-2.0
// Copyright (c) HashiCorp, Inc.
// SPDX-License-Identifier: MPL-2.0

package schemamd_test

import (
	"encoding/json"
	"os"
	"strings"
	"testing"

	"github.com/google/go-cmp/cmp"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-plugin-docs/schemamd"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-plugin-sdk/internal-shared/schema"
)

func TestRender(t *testing.T) {
	t.Parallel()

	for _, c := range []struct {
		name         string
		inputFile    string
		expectedFile string
	}{
		{
			"plugin config",
			"testdata/plugin_config.json",
			"testdata/plugin_config.md",
		},
	} {
		c := c
		t.Run(c.name, func(t *testing.T) {
			t.Parallel()

			input, err := os.ReadFile(c.inputFile)
			if err != nil {
				t.Fatal(err)
			}

			expected, err := os.ReadFile(c.expectedFile)
			if err != nil {
				t.Fatal(err)
			}

			var schema schema.PluginSchema

			err = json.Unmarshal(input, &schema)
			if err != nil {
				t.Fatal(err)
			}

			b := &strings.Builder{}
			err = schemamd.RenderPlugin(&schema, b)
			if err != nil {
				t.Fatal(err)
			}

			// Remove \r characters so tests don't fail on windows
			expectedStr := strings.ReplaceAll(string(expected), "\r", "")

			// Remove trailing newlines before comparing (some text editors remove them).
			expectedStr = strings.TrimRight(expectedStr, "\n")
			actual := strings.TrimRight(b.String(), "\n")
			if diff := cmp.Diff(expectedStr, actual); diff != "" {
				t.Fatalf("Unexpected diff (-wanted, +got): %s", diff)
			}
		})
	}
}
