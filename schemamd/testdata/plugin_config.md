## Schema

### Inputs

#### Required

- `field1` (Boolean) This is a description of field1
- `test_block_list` (Block List, Required) This is a test block list (see [below for nested schema](#nestedblock--test_block_list))
- `test_block_map` (Block Map, Required) (see [below for nested schema](#nestedblock--test_block_map))
- `test_block_required` (Block, Required) This is a test block (see [below for nested schema](#nestedblock--test_block_required))

#### Optional

- `field2` (String) This is a description of field2
- `test_block_optional` (Block, Optional) This is a test block (see [below for nested schema](#nestedblock--test_block_optional))

<a id="nestedblock--test_block_list"></a>
#### Nested Schema for `test_block_list`

Required:

- `foo` (String) This is a test block attribute


<a id="nestedblock--test_block_map"></a>
#### Nested Schema for `test_block_map`

Required:

- `foo` (String) This is a test block attribute


<a id="nestedblock--test_block_required"></a>
#### Nested Schema for `test_block_required`

Required:

- `foo` (String) This is a test block attribute


<a id="nestedblock--test_block_optional"></a>
#### Nested Schema for `test_block_optional`

Required:

- `foo` (String) This is a test block attribute