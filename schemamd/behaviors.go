// Copyright (c) The Phobos Authors
// SPDX-License-Identifier: MPL-2.0
// Copyright (c) HashiCorp, Inc.
// SPDX-License-Identifier: MPL-2.0

package schemamd

import (
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-plugin-sdk/internal-shared/component"
)

func childAttributeIsRequired(att *component.SchemaAttribute) bool {
	return att.Required == nil || *att.Required
}

func childAttributeIsOptional(att *component.SchemaAttribute) bool {
	return att.Required != nil && !*att.Required
}

func childBlockIsRequired(block *component.SchemaBlockSpec) bool {
	return block.Required
}

func childBlockIsOptional(block *component.SchemaBlockSpec) bool {
	return !block.Required
}
