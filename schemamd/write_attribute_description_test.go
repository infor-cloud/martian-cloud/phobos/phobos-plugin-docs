// Copyright (c) The Phobos Authors
// SPDX-License-Identifier: MPL-2.0
// Copyright (c) HashiCorp, Inc.
// SPDX-License-Identifier: MPL-2.0

package schemamd_test

import (
	"strings"
	"testing"

	"github.com/aws/smithy-go/ptr"
	"github.com/google/go-cmp/cmp"
	"github.com/zclconf/go-cty/cty"

	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-plugin-docs/schemamd"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-plugin-sdk/internal-shared/component"
)

func TestWriteAttributeDescription(t *testing.T) {
	t.Parallel()

	for _, c := range []struct {
		expected string
		att      *component.SchemaAttribute
	}{
		// required
		{
			"(String, Required) This is an attribute.",
			&component.SchemaAttribute{
				Type:        cty.String,
				Required:    ptr.Bool(true),
				Description: "This is an attribute.",
			},
		},
		{
			"(String, Required, Deprecated) This is an attribute.",
			&component.SchemaAttribute{
				Type:        cty.String,
				Required:    ptr.Bool(true),
				Description: "This is an attribute.",
				Deprecated:  true,
			},
		},
		{
			"(String, Optional) This is an attribute.",
			&component.SchemaAttribute{
				Type:        cty.String,
				Required:    ptr.Bool(false),
				Description: "This is an attribute.",
			},
		},

		// white space in descriptions
		{
			"(String, Required) This is an attribute.",
			&component.SchemaAttribute{
				Type:        cty.String,
				Required:    ptr.Bool(true),
				Description: " This is an attribute.",
			},
		},
		{
			"(String, Required) This is an attribute.",
			&component.SchemaAttribute{
				Type:        cty.String,
				Required:    ptr.Bool(true),
				Description: "This is an attribute. ",
			},
		},
		{
			"(String, Required) This is an attribute.",
			&component.SchemaAttribute{
				Type:        cty.String,
				Required:    ptr.Bool(true),
				Description: "\n\t This is an attribute.\n\t ",
			},
		},
	} {
		c := c
		t.Run(c.expected, func(t *testing.T) {
			t.Parallel()

			b := &strings.Builder{}
			err := schemamd.WriteAttributeDescription(b, c.att, true)
			if err != nil {
				t.Fatal(err)
			}
			actual := b.String()
			if diff := cmp.Diff(c.expected, actual); diff != "" {
				t.Fatalf("Unexpected diff (-wanted, +got): %s", diff)
			}
		})
	}
}
