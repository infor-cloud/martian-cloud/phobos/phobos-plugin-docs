// Copyright (c) The Phobos Authors
// SPDX-License-Identifier: MPL-2.0
// Copyright (c) HashiCorp, Inc.
// SPDX-License-Identifier: MPL-2.0

// Package schemamd provides a function to render a Markdown formatted Schema definition.
package schemamd

import (
	"fmt"
	"io"
	"sort"
	"strings"

	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-plugin-sdk/internal-shared/component"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-plugin-sdk/internal-shared/schema"
)

// RenderPlugin writes a Markdown formatted Schema definition to the specified writer.
// A Schema contains a Version and the root Block, for example:
func RenderPlugin(schema *schema.PluginSchema, w io.Writer) error {
	_, err := io.WriteString(w, "## Schema\n\n")
	if err != nil {
		return err
	}

	err = writeRootBlock(w, schema.PipelineSchema.Config)
	if err != nil {
		return fmt.Errorf("unable to render schema: %w", err)
	}

	return nil
}

// RenderAction renders the markdown for an action schema.
func RenderAction(schema *component.PipelinePluginActionSchema, w io.Writer) error {
	_, err := io.WriteString(w, "## Schema\n\n")
	if err != nil {
		return err
	}

	err = writeRootBlock(w, schema.Input)
	if err != nil {
		return fmt.Errorf("unable to render schema: %w", err)
	}

	if schema.Outputs != nil {
		_, err = io.WriteString(w, "### Outputs\n\n")
		if err != nil {
			return err
		}

		sortedNames := []string{}
		for n := range schema.Outputs {
			sortedNames = append(sortedNames, n)
		}
		sort.Strings(sortedNames)

		nestedTypes := []nestedType{}
		for _, name := range sortedNames {
			attr := schema.Outputs[name]
			nt, err := writeAttribute(w, []string{name}, attr, groupFilter{})
			if err != nil {
				return fmt.Errorf("unable to render output %q: %w", name, err)
			}
			nestedTypes = append(nestedTypes, nt...)
		}

		io.WriteString(w, "\n\n")

		if err = writeNestedTypes(w, nestedTypes); err != nil {
			return err
		}
	}

	return nil
}

// Group by Attribute/Block characteristics.
type groupFilter struct {
	topLevelTitle string
	nestedTitle   string

	filterAttribute func(att *component.SchemaAttribute) bool
	filterBlock     func(block *component.SchemaBlockSpec) bool
}

var (
	// Attributes and Blocks are in one of 3 characteristic groups:
	// * Required
	// * Optional
	groupFilters = []groupFilter{
		{"#### Required", "Required:", childAttributeIsRequired, childBlockIsRequired},
		{"#### Optional", "Optional:", childAttributeIsOptional, childBlockIsOptional},
	}
)

type nestedType struct {
	anchorID         string
	path             []string
	block            *component.SchemaBlock
	group            groupFilter
	nestedAttributes map[string]*component.SchemaAttribute
}

func writeAttribute(w io.Writer, path []string, att *component.SchemaAttribute, group groupFilter) ([]nestedType, error) {
	name := path[len(path)-1]

	_, err := io.WriteString(w, "- `"+name+"` ")
	if err != nil {
		return nil, err
	}

	err = WriteAttributeDescription(w, att, false)
	if err != nil {
		return nil, err
	}
	if att.Type.IsTupleType() {
		return nil, fmt.Errorf("tuples are not supported")
	}

	anchorID := "nestedatt--" + strings.Join(path, "--")
	nestedTypes := []nestedType{}
	switch {
	case att.Type.IsObjectType():
		_, err = io.WriteString(w, " (see [below for nested schema](#"+anchorID+"))")
		if err != nil {
			return nil, err
		}

		nestedTypes = append(nestedTypes, nestedType{
			anchorID:         anchorID,
			path:             path,
			group:            group,
			nestedAttributes: att.NestedAttributes,
		})
	case att.Type.IsCollectionType() && att.Type.ElementType().IsObjectType():
		_, err = io.WriteString(w, " (see [below for nested schema](#"+anchorID+"))")
		if err != nil {
			return nil, err
		}

		nestedTypes = append(nestedTypes, nestedType{
			anchorID:         anchorID,
			path:             path,
			group:            group,
			nestedAttributes: att.NestedAttributes,
		})
	}

	_, err = io.WriteString(w, "\n")
	if err != nil {
		return nil, err
	}

	return nestedTypes, nil
}

func writeBlockType(w io.Writer, path []string, block *component.SchemaBlockSpec) ([]nestedType, error) {
	name := path[len(path)-1]

	_, err := io.WriteString(w, "- `"+name+"` ")
	if err != nil {
		return nil, err
	}

	err = WriteBlockTypeDescription(w, block)
	if err != nil {
		return nil, fmt.Errorf("unable to write block description for %q: %w", name, err)
	}

	anchorID := "nestedblock--" + strings.Join(path, "--")
	nt := nestedType{
		anchorID: anchorID,
		path:     path,
		block:    block.Block,
	}

	_, err = io.WriteString(w, " (see [below for nested schema](#"+anchorID+"))")
	if err != nil {
		return nil, err
	}

	_, err = io.WriteString(w, "\n")
	if err != nil {
		return nil, err
	}

	return []nestedType{nt}, nil
}

func writeRootBlock(w io.Writer, block *component.SchemaBlock) error {
	io.WriteString(w, "### Inputs\n\n")
	return writeBlockChildren(w, nil, block, true)
}

func writeBlockChildren(w io.Writer, parents []string, block *component.SchemaBlock, root bool) error {
	names := []string{}
	for n := range block.Attributes {
		names = append(names, n)
	}
	for n := range block.BlockSpecs {
		names = append(names, n)
	}

	groups := map[int][]string{}

	// Group Attributes/Blocks by characteristics.
nameLoop:
	for _, n := range names {
		if childBlock, ok := block.BlockSpecs[n]; ok {
			for i, gf := range groupFilters {
				if gf.filterBlock(childBlock) {
					groups[i] = append(groups[i], n)
					continue nameLoop
				}
			}
		} else if childAtt, ok := block.Attributes[n]; ok {
			for i, gf := range groupFilters {
				if gf.filterAttribute(childAtt) {
					groups[i] = append(groups[i], n)
					continue nameLoop
				}
			}
		}

		return fmt.Errorf("no match for %q, this can happen if you have incompatible schema defined, for example an "+
			"optional block where all the child attributes are computed, in which case the block itself should also "+
			"be marked computed", n)
	}

	nestedTypes := []nestedType{}

	for i, gf := range groupFilters {
		sortedNames := groups[i]
		if len(sortedNames) == 0 {
			continue
		}
		sort.Strings(sortedNames)

		groupTitle := gf.topLevelTitle
		if !root {
			groupTitle = gf.nestedTitle
		}

		_, err := io.WriteString(w, groupTitle+"\n\n")
		if err != nil {
			return err
		}

		for _, name := range sortedNames {
			path := make([]string, len(parents), len(parents)+1)
			copy(path, parents)
			path = append(path, name)

			if childBlock, ok := block.BlockSpecs[name]; ok {
				nt, err := writeBlockType(w, path, childBlock)
				if err != nil {
					return fmt.Errorf("unable to render block %q: %w", name, err)
				}

				nestedTypes = append(nestedTypes, nt...)
				continue
			} else if childAtt, ok := block.Attributes[name]; ok {
				nt, err := writeAttribute(w, path, childAtt, gf)
				if err != nil {
					return fmt.Errorf("unable to render attribute %q: %w", name, err)
				}

				nestedTypes = append(nestedTypes, nt...)
				continue
			}

			return fmt.Errorf("unexpected name in schema render %q", name)
		}

		_, err = io.WriteString(w, "\n")
		if err != nil {
			return err
		}
	}

	err := writeNestedTypes(w, nestedTypes)
	if err != nil {
		return err
	}

	return nil
}

func writeNestedTypes(w io.Writer, nestedTypes []nestedType) error {
	for _, nt := range nestedTypes {
		_, err := io.WriteString(w, "<a id=\""+nt.anchorID+"\"></a>\n")
		if err != nil {
			return err
		}

		_, err = io.WriteString(w, "#### Nested Schema for `"+strings.Join(nt.path, ".")+"`\n\n")
		if err != nil {
			return err
		}

		switch {
		case nt.block != nil:
			err = writeBlockChildren(w, nt.path, nt.block, false)
			if err != nil {
				return err
			}
		case nt.nestedAttributes != nil:
			err = writeObjectChildren(w, nt.path, nt.nestedAttributes, nt.group)
			if err != nil {
				return err
			}
		default:
			return fmt.Errorf("missing information on nested block: %s", strings.Join(nt.path, "."))
		}

		_, err = io.WriteString(w, "\n")
		if err != nil {
			return err
		}
	}

	return nil
}

func writeObjectAttribute(w io.Writer, path []string, att *component.SchemaAttribute, group groupFilter) ([]nestedType, error) {
	name := path[len(path)-1]

	_, err := io.WriteString(w, "- `"+name+"` ")
	if err != nil {
		return nil, err
	}

	err = WriteAttributeDescription(w, att, att.Required != nil)
	if err != nil {
		return nil, err
	}

	anchorID := "nestedobjatt--" + strings.Join(path, "--")
	nestedTypes := []nestedType{}
	switch {
	case att.Type.IsObjectType():
		_, err = io.WriteString(w, " (see [below for nested schema](#"+anchorID+"))")
		if err != nil {
			return nil, err
		}

		nestedTypes = append(nestedTypes, nestedType{
			anchorID:         anchorID,
			path:             path,
			nestedAttributes: att.NestedAttributes,
			group:            group,
		})
	case att.Type.IsCollectionType() && att.Type.ElementType().IsObjectType():
		_, err = io.WriteString(w, " (see [below for nested schema](#"+anchorID+"))")
		if err != nil {
			return nil, err
		}

		nestedTypes = append(nestedTypes, nestedType{
			anchorID:         anchorID,
			path:             path,
			nestedAttributes: att.NestedAttributes,
			group:            group,
		})
	}

	_, err = io.WriteString(w, "\n")
	if err != nil {
		return nil, err
	}

	return nestedTypes, nil
}

func writeObjectChildren(w io.Writer, parents []string, children map[string]*component.SchemaAttribute, group groupFilter) error {
	_, err := io.WriteString(w, group.nestedTitle+"\n\n")
	if err != nil {
		return err
	}

	sortedNames := []string{}
	for n := range children {
		sortedNames = append(sortedNames, n)
	}
	sort.Strings(sortedNames)
	nestedTypes := []nestedType{}

	for _, name := range sortedNames {
		att := children[name]
		path := append(parents, name)

		nt, err := writeObjectAttribute(w, path, att, group)
		if err != nil {
			return fmt.Errorf("unable to render attribute %q: %w", name, err)
		}

		nestedTypes = append(nestedTypes, nt...)
	}

	_, err = io.WriteString(w, "\n")
	if err != nil {
		return err
	}

	err = writeNestedTypes(w, nestedTypes)
	if err != nil {
		return err
	}

	return nil
}
