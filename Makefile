VERSION ?= $(shell git describe --tags --always --dirty --match=v* 2> /dev/null || echo "1.0.0")
PACKAGES := $(shell go list ./... | grep -v /vendor/)
LDFLAGS := -ldflags "-X build.Version=${VERSION}"

build:
	CGO_ENABLED=0 go build ${LDFLAGS} -a -o phobosplugindocs ./cmd

.PHONY: lint
lint: ## run golint on all Go package
	@revive -set_exit_status $(PACKAGES)

.PHONY: vet
vet: ## run golint on all Go package
	@go vet $(PACKAGES)

.PHONY: fmt
fmt: ## run "go fmt" on all Go packages
	@go fmt $(PACKAGES)

.PHONY: test
test: ## run unit tests
	go test ./...

.PHONY: generate
generate: ## run go generate
	go generate -v ./...
